import './App.css'
import { useQuery } from "@apollo/react-hooks"
import gql from "graphql-tag"

const GET_USERS = gql`
  {
    patient {
      first_name
      last_name
      email
      gender
      ip_address
    }
  }
`

const Appointment = ({ patient: { first_name, last_name, email, gender} }) => (
  <div className="Card">
    <div>
      <h3>Full Name: {first_name} {last_name}</h3>
      <p>Gender :  {gender}</p>
      <p>email :  {email}</p>
    </div>
    
  </div>
)

function App() {
  const { loading, error, data } = useQuery(GET_USERS)

  if (error) return <h1>Something went wrong!</h1>
  if (loading) return <h1>Loading...</h1>

  return (
    <main className="App">
      <h1>Appointment List</h1>
      {data.patient.map(patient => (
        <Appointment key={patient.name} patient={patient} />
      ))}
    </main>
  )
}

export default App
